package cmd

import (
	"encoding/json"
	"os"
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
)

var dataPath string

func addDataFlag(cmd *cobra.Command, required bool) {
	cmd.Flags().StringVar(&dataPath, "data", "", "Path to a file containing the JSON structure to send")
	if required {
		if err := cmd.MarkFlagRequired("data"); err != nil {
			logging.Error().Fatalln(err)
		}
	}
}

func readData[T interface{}]() T {
	f, err := os.Open(dataPath)
	if err != nil {
		logging.Error().Fatalf("Failed to open file %s: %s", dataPath, err)
	}
	defer f.Close()

	dec := json.NewDecoder(f)

	var output T
	if err := dec.Decode(&output); err != nil {
		logging.Error().Fatalf("Failed to decode input payload: %v", err)
	}
	return output
}
