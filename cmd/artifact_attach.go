// tuleap-cli, a CLI client to Tuleap
// Copyright (C) 2024 - CS GROUP - France
// Author: Guilhem Bonnefille

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"tuleap/tuleap-cli/internal/logging"

	"gitlab.com/csgroup-oss/go-tuleap"

	"github.com/spf13/cobra"
)

var attachOps struct {
	file_path  string
	field_name string
}

func init() {
	artifactsCmd.AddCommand(artifactAttachCmd)

	addIdFlag(artifactAttachCmd, true)

	artifactAttachCmd.Flags().StringVar(&attachOps.file_path, "file", "", "Path to the file to attach")
	if err := artifactAttachCmd.MarkFlagRequired("file"); err != nil {
		logging.Error().Fatalln(err)
	}
	artifactAttachCmd.Flags().StringVar(&attachOps.field_name, "field-name", "attachment", "Name of the field where to attach the file to")
}

var artifactAttachCmd = &cobra.Command{
	Use:     "attach --id id --file file [--field-name field-name]",
	Short:   "Attach a file to an artifact.",
	Example: "tuleap-cli -s tuleap.net artifacts --id 35499 attach --file-path report.log",
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		artifact, err := tc.GetArtifact(id)
		if err != nil {
			logging.Error().Fatalf("Failed to get artifact: %v", err)
		}

		logging.Debug().Printf("Attaching file %s to artifact %d on %s", attachOps.file_path, id, attachOps.field_name)
		tracker, err := tc.GetTracker(artifact.Tracker.Id)
		if err != nil {
			logging.Error().Fatalf("Failed to get tracker: %v", err)
		}
		logging.Debug().Println("Tracker", tracker)

		tracker_field := tracker.GetTrackerFieldByName(attachOps.field_name)
		if tracker_field == nil {
			logging.Error().Fatalf("Field %s not found for tracker %d", attachOps.field_name, artifact.Tracker.Id)
		}
		logging.Debug().Println("Tracker field", tracker_field)

		field := artifact.GetValueById(tracker_field.Id)
		if field == nil {
			logging.Error().Fatalf("Field %s not found for artifact %d", attachOps.field_name, id)
		}
		logging.Debug().Printf("Artifact Field %+v\n", field)

		file_name := filepath.Base(attachOps.file_path)
		logging.Debug().Println("file name", file_name)

		// Check presence
		if field != nil {
			// Field already exist
			// Look for attached files
			if field.ContainsFile(file_name) {
				logging.Warn().Fatalf("File %s is already attached in artifact %d", file_name, id)
			}
		}

		// Get file size from file system
		file_size, err := getFileSize(attachOps.file_path)
		if err != nil {
			logging.Error().Fatalf("Failed to compute size of file %s: %s", attachOps.file_path, err)
		}

		rc, err := os.Open(attachOps.file_path)
		if err != nil {
			logging.Error().Fatalf("Failed to open file '%s': %s", attachOps.file_path, err)
		}
		defer rc.Close()

		file_info, err := tc.CreateTrackerFieldFile(tracker_field.Id, file_name, file_size, "application/octet-stream")
		if err != nil {
			logging.Error().Fatalf("Failed to create file %s in field %s of artifact %d: %s", file_name, attachOps.field_name, id, err)
		}
		logging.Debug().Println("File info", file_info)

		err = tc.UploadFile(file_info.UpHref, file_name, rc, file_size)
		if err != nil {
			logging.Error().Fatalf("Failed to upload file %s at %s: %s", file_name, file_info.UpHref, err)
		}

		if field == nil {
			field = &tuleap.ArtifactValue{
				Id:    tracker_field.Id,
				Value: make([]int, 0),
			}
		}
		err = tc.AppendFileToArtifact(id, field, file_info.Id)
		if err != nil {
			logging.Error().Fatalf("Failed to append file %s to artifact %d: %s", file_name, id, err)
		}
	},
}

// getFileSize returns the size of a file
func getFileSize(filePath string) (int64, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return 0, fmt.Errorf("Failed to open file '%s': %w", filePath, err)
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		return 0, err
	}
	return fi.Size(), nil
}
