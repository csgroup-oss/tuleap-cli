// tuleap-cli, a CLI client to Tuleap
// Copyright (C) 2024 - CS GROUP - France
// Author: Guilhem Bonnefille

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
	"gitlab.com/csgroup-oss/go-tuleap"
)

func init() {
	projectsCmd.AddCommand(projectsFrsPackagesCmd)

	addIdFlag(projectsFrsPackagesCmd, true)
}

var projectsFrsPackagesCmd = &cobra.Command{
	Use:   "frs_packages --id id",
	Short: "List FRS Packages of a given project",
	Long:  "List all FRS Packages of a given project in ndjson format.",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		logging.Info().Printf("Looking for FRS packages of project %d", id)
		runMulti(func(tc *tuleap.Client) <-chan tuleap.FrsPackage {
			return tc.GetProjectFrsPackagesAll(id, errorLogger)
		})
	},
}
