// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
	"gitlab.com/csgroup-oss/go-tuleap"
)

func init() {
	artifactsCmd.AddCommand(artifactChangesetsCmd)

	addIdFlag(artifactChangesetsCmd, true)
}

var artifactChangesetsCmd = &cobra.Command{
	Use:     "changesets --id id",
	Short:   "Export changesets of an artifact.",
	Long:    "Export all changesets of an artifact in ndjson format.",
	Example: "tuleap-cli -s tuleap.net artifacts --id 35499	changesets",
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		logging.Info().Printf("Looking for changesets of artifact %d", id)
		runMulti(func(tc *tuleap.Client) <-chan interface{} {
			return tc.GetArtifactChangesetsRawAll(id, errorLogger)
		})
	},
}
