// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
	"gitlab.com/csgroup-oss/go-tuleap"
)

func init() {
	rootCmd.AddCommand(frsFilesCmd)

	addIdFlag(frsFilesCmd, true)
}

var frsFilesCmd = &cobra.Command{
	Use:     "frs_files --id id",
	Short:   "Export a single FRS file",
	Example: "tuleap-cli -s tuleap.net frs_files --id 148",
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		logging.Info().Printf("Looking for FRS file %d", id)
		runSingle(func(tc *tuleap.Client) (interface{}, error) {
			return tc.GetFrsFile(id)
		})
	},
}
