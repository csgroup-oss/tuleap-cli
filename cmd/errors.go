package cmd

import (
	"tuleap/tuleap-cli/internal/logging"

	"gitlab.com/csgroup-oss/go-tuleap"
)

type ErrorLogger struct {
}

func (e ErrorLogger) Error(err error) error {
	logging.Error().Fatal(err)
	return nil
}

// Type control
var errorLogger tuleap.ErrorReceiver = ErrorLogger{}
