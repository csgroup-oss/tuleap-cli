// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"os"
	"path/filepath"
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
)

var fileCreateOptions struct {
	file_path string
	name      string
	release   int
}

func init() {
	frsFilesCmd.AddCommand(frsFilesCreateCmd)

	frsFilesCreateCmd.Flags().StringVar(&fileCreateOptions.file_path, "file", "", "Path to the file to upload")
	if err := frsFilesCreateCmd.MarkFlagRequired("file"); err != nil {
		logging.Error().Fatalln(err)
	}
	frsFilesCreateCmd.Flags().StringVar(&fileCreateOptions.name, "name", "", "Name of the file to upload")

	frsFilesCreateCmd.Flags().IntVar(&fileCreateOptions.release, "release-id", 0, "Release id")
	if err := frsFilesCreateCmd.MarkFlagRequired("release-id"); err != nil {
		logging.Error().Fatalln(err)
	}
}

var frsFilesCreateCmd = &cobra.Command{
	Use:     "create --release-id id --file file [--name name]",
	Short:   "Create a single FRS file",
	Long:    "Create a single FRS file in a given release",
	Example: "tuleap-cli -s tuleap.net frs_files --release-id 148 --file my-project.zip",
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		// Set a default value for the name if not provided
		if fileCreateOptions.name == "" {
			fileCreateOptions.name = filepath.Base(fileCreateOptions.file_path)
		}

		logging.Info().Printf("Creating FRS file '%s' on release %d with content from '%s'", fileCreateOptions.name, id, fileCreateOptions.file_path)

		// Get file size from file system
		file_size, err := getFileSize(fileCreateOptions.file_path)
		if err != nil {
			logging.Error().Fatalf("Failed to compute size of file %s: %s", fileCreateOptions.file_path, err)
		}

		upload, err := tc.CreateFrsFile(fileCreateOptions.release, fileCreateOptions.name, file_size)
		if err != nil {
			logging.Error().Fatalf("Failed to create file %s: %s", fileCreateOptions.name, err)
		}

		rc, err := os.Open(fileCreateOptions.file_path)
		if err != nil {
			logging.Error().Fatalf("Failed to open file %s: %s", fileCreateOptions.file_path, err)
		}
		defer rc.Close()

		err = tc.UploadFile(upload.UpHref, fileCreateOptions.name, rc, file_size)
		if err != nil {
			logging.Error().Fatalf("Failed to upload file %s: %s", fileCreateOptions.file_path, err)
		}
	},
}
