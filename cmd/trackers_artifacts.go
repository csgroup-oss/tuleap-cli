// tuleap-cli, a CLI client to Tuleap
// Copyright (C) 2024 - CS GROUP - France
// Author: Guilhem Bonnefille

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
	"gitlab.com/csgroup-oss/go-tuleap"
)

var trackersOpts struct {
	values       string
	query        string
	expert_query string
}

func init() {
	trackersCmd.AddCommand(trackersArtifactsCmd)

	addIdFlag(trackersArtifactsCmd, true)
	trackersArtifactsCmd.Flags().StringVar(&trackersOpts.values, "values", "", "values option")
	// FIXME trackersArtifactsCmd.Flags().StringVar(&trackersOpts.query, "query", "", "query option")
	trackersArtifactsCmd.Flags().StringVar(&trackersOpts.expert_query, "expert-query", "", "expert query option")
}

var trackersArtifactsCmd = &cobra.Command{
	Use:   "artifacts --id id [--values values] [--expert-query query]",
	Short: "Export artifacts of a tracker",
	Long: `Export all artifacts of a tracker matching the query with the requested level of detail, in ndjson format.

The query parameter is a tuleap query string ([TQL](https://docs.tuleap.org/user-guide/tql.html)).`,
	Example: `tuleap-cli -s tuleap.net trackers --id 140 artifacts --expert-query "status_id = 'New'"`,
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		logging.Info().Printf("Looking for artifacts of the tracker %d", id)
		runMulti(func(tc *tuleap.Client) <-chan interface{} {
			return tc.GetTrackerArtifactsAllRaw(id, trackersOpts.values, trackersOpts.expert_query, errorLogger)
		})
	},
}
