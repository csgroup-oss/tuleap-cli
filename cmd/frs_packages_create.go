// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
)

func init() {
	frsPackagesCmd.AddCommand(frsPackagesCreateCmd)

	addDataFlag(frsPackagesCreateCmd, true)
}

var frsPackagesCreateCmd = &cobra.Command{
	Use:   "create --data file",
	Short: "Create a new FRS package",
	Long: `This command creates a new FRS package.
	
The new Package should be given as a JSON file via the --data flag.`,
	Example: `tuleap frs-packages create --data '{"project_id": 1,"label": "My FRS package"}`,
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		logging.Info().Printf("Creating new FRS package from '%s'", dataPath)

		output := readData[interface{}]()

		pack, err := tc.CreateFrsPackage(output)
		if err != nil {
			logging.Error().Fatalf("Failed to create FRS package: %s", err)
		}

		dumpSingle(pack)
	},
}
