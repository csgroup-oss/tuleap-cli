// tuleap-cli, a CLI client to Tuleap
// Copyright (C) 2024 - CS GROUP - France
// Author: Guilhem Bonnefille

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"strings"
	"syscall"
	"tuleap/tuleap-cli/internal/logging"

	"gitlab.com/csgroup-oss/go-tuleap"

	"github.com/spf13/cobra"
	"golang.org/x/term"
)

var (
	// Used for flags.
	verbose bool
	debug   bool

	insecure bool

	server       string
	auth         tuleap.ClientAuth
	readPassword bool

	outputSpec string
	output     *os.File

	paginated bool
	path      string
	query     []string
	method    string

	tc *tuleap.Client

	version string = "unset"

	rootCmd = &cobra.Command{
		Use:   "tuleap-cli -s server [--path path] [--query query] [--paginated] [--output output]",
		Short: "A collection of tools to help exporting derived data from Tuleap's trackers",
		Long:  `The query parameter is a tuleap query string ([TQL](https://docs.tuleap.org/user-guide/tql.html)).`,
		Example: `tuleap-cli -s tuleap.net --path /api/version
tuleap-cli -s tuleap.net --path /api/projects --paginated
tuleap-cli -s tuleap.net --path /api/trackers/140/artifacts --query 'expert_query=status_id = "New"' --paginated
tuleap-cli -s tuleap.net --path /api/artifacts/1234 --method PUT --data change.json`,
		Version: version,
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			logging.Configure(debug, verbose)
			output = getOutput()
			if readPassword {
				auth.Password = requestPassword()
			}
			// Create global client
			tc = newTuleapClient()
		},
		PersistentPostRun: func(cmd *cobra.Command, args []string) {
			output.Close()
		},
		Run: func(cmd *cobra.Command, args []string) {
			v := url.Values{}
			for _, q := range query {
				key, val, found := strings.Cut(q, "=")
				if found {
					v.Set(key, val)
				} else {
					logging.Error().Fatalf("Failed to find separator '=' in %s", q)
				}
			}

			var input interface{}
			if dataPath == "" {
				logging.Debug().Printf("No file selected as input")
			} else {
				input = readData[interface{}]()
			}

			if paginated {
				runMulti(func(tc *tuleap.Client) <-chan interface{} {
					return tuleap.RequestPaginatedJSONAll[interface{}](tc, method, path, &v, input, errorLogger)
				})
			} else {
				runSingle(func(tc *tuleap.Client) (interface{}, error) {
					return tuleap.RequestJSON[interface{}](tc, method, path, &v, input)
				})
			}
		},
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "activate debug messages")
	rootCmd.PersistentFlags().BoolVar(&verbose, "verbose", false, "make the output more verbose")

	rootCmd.PersistentFlags().StringVarP(&server, "server", "s", "tuleap.net", "the server's hostname")
	if err := rootCmd.MarkPersistentFlagRequired("server"); err != nil {
		logging.Error().Fatalln(err)
	}

	rootCmd.PersistentFlags().StringVarP(&auth.Token, "token", "t", "", "the user's token")
	rootCmd.PersistentFlags().StringVarP(&auth.Username, "username", "u", "", "the user's name")
	rootCmd.PersistentFlags().StringVarP(&auth.Password, "password", "p", "", "the user's password")
	rootCmd.PersistentFlags().BoolVarP(&readPassword, "password-read", "P", false, "read the user's password from standard input")

	rootCmd.PersistentFlags().BoolVarP(&insecure, "insecure", "k", false, "ignore ssl error")

	rootCmd.Flags().BoolVar(&paginated, "paginated", false, "endpoint is paginated")
	rootCmd.Flags().StringVar(&path, "path", "", "endpoint path")
	if err := rootCmd.MarkFlagRequired("path"); err != nil {
		logging.Error().Fatalln(err)
	}
	rootCmd.Flags().StringArrayVar(&query, "query", nil, "query parameters in form 'key=value'")

	rootCmd.PersistentFlags().StringVarP(&outputSpec, "output", "o", "-", "the file where to write data, '-' means standard output")

	addDataFlag(rootCmd, false)

	rootCmd.Flags().StringVar(&method, "method", "GET", "the user's token")
}

func getOutput() *os.File {
	if outputSpec == "-" {
		return os.Stdout
	} else {
		f, err := os.Create(outputSpec)
		if err != nil {
			logging.Error().Fatalf("Failed to open file '%s': %v", outputSpec, err)
		}
		return f
	}
}

func requestPassword() string {
	fmt.Fprint(os.Stderr, "Password: ")
	bytePassword, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		logging.Error().Fatalf("Failed to read password: %v", err)
	}
	fmt.Println()

	password := string(bytePassword)
	return password
}

func newTuleapClient() *tuleap.Client {
	//nolint:gosec // G402: TLS InsecureSkipVerify set true. (gosec)
	tc, err := tuleap.NewClient(server, auth, &tls.Config{InsecureSkipVerify: insecure})
	if err != nil {
		logging.Error().Fatalf("Failed to access Tuleap: %v", err)
	}
	if debug {
		tc.SetTransport(&logging.LoggingTransport{})
	}
	return tc
}

// dumpSingle dumps a single value.
func dumpSingle(data interface{}) {
	if data == nil {
		return
	}
	// JSON
	encoder := json.NewEncoder(output)
	// Encode as JSON
	if err := encoder.Encode(data); err != nil {
		logging.Error().Fatalf("Failed to encode data: %v", err)
	}
}

// runSingle runs a request that returns a single value.
func runSingle(fct func(tc *tuleap.Client) (interface{}, error)) {
	data, err := fct(tc)
	if err != nil {
		logging.Error().Fatalf("Failed to retrieve data: %v", err)
	}
	dumpSingle(data)
}

// dumpMulti dumps a stream of values.
func dumpMulti[T interface{}](data <-chan T) {
	// JSON
	encoder := json.NewEncoder(output)
	for datum := range data {
		// Encode as JSON
		if err := encoder.Encode(datum); err != nil {
			logging.Error().Fatalf("Failed to encode data: %v", err)
		}
	}
}

// runMulti runs a request that returns a stream of values.
func runMulti[T interface{}](fct func(tc *tuleap.Client) <-chan T) {
	data := fct(tc)
	dumpMulti(data)
}

// GetCmd returns the root command.
func GetCmd() *cobra.Command {
	return rootCmd
}
