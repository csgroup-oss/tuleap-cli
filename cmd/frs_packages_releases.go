// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"tuleap/tuleap-cli/internal/logging"

	"github.com/spf13/cobra"
	"gitlab.com/csgroup-oss/go-tuleap"
)

func init() {
	frsPackagesCmd.AddCommand(frsPackagesReleasesCmd)

	addIdFlag(frsPackagesReleasesCmd, true)
}

var frsPackagesReleasesCmd = &cobra.Command{
	Use:     "releases --id id",
	Short:   "List releases of a given FRS package",
	Example: "tuleap-cli -s tuleap.net frs_packages --id 1 releases",
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		logging.Info().Printf("Looking for FRS releases of package %d", id)
		runMulti(func(tc *tuleap.Client) <-chan tuleap.FrsRelease {
			return tc.GetFrsPackageReleasesAll(id, errorLogger)
		})
	},
}
