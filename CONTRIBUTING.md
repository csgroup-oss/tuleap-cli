# How to contribute

We'd love to accept your patches and contributions to this project. There are a just a few small guidelines you need to follow.

## Reporting issues

Bugs, feature requests, and development-related questions should be directed to
our [issue tracker](https://gitlab.com/csgroup-oss/tuleap-cli/issues).  If
reporting a bug, please try and provide as much context as possible such as
your operating system, Go version, and anything else that might be relevant to
the bug.  For feature requests, please explain what you're trying to do, and
how the requested feature would help you do that.

## Contributing Code

Pull requests are always welcome. When in doubt if your contribution fits within
the rest of the project, feel free to first open an issue to discuss your idea.

This is not needed when fixing a bug or adding an enhancement, as long as the
enhancement you are trying to add can be found in the public Tuleap API docs as
this project only supports what is in the public API docs.

## Generating doc

The manual is generated automatically from the embedded documentation in the cobra.Command.
Simply run `make gendoc` and commit changes.