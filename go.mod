module tuleap/tuleap-cli

require (
	github.com/spf13/cobra v1.6.1
	github.com/spf13/jwalterweatherman v1.1.0
	gitlab.com/csgroup-oss/go-tuleap v0.0.0-20240206221754-73e8b773ada2
	golang.org/x/term v0.5.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/eventials/go-tus v0.0.0-20220610120217-05d0564bb571 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.5.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

go 1.18
