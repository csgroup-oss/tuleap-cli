# tuleap-cli

A command line oriented interface (CLI) to [Tuleap](https://www.tuleap.org/).

Related to a simple `curl` invocation of the Tuleap's API, this tool simplify:

- the authentication
- the iteration on paginated endpoints

## Usage

### Authentication

In order to authenticate against the server, it is possible to use login and password (`--username` & `--password`) or token (`--token`).
The first solution is useful for interactive sessions, while the second should be preferred for batch.
The personal token should have the API scope.

In a truly interactive session, it could be better to pass the password interactively.
In such situation, use `-P` instead of `-p`.

### Generic usage

The utility allow to request any endpoint with the following:

```SH
tuleap-cli -s {server} --path {path} [--query {key}={value}] [--paginated]
```

where:

* `server` is the server's hostname
* `path` is the path part of the endpoint
* `query` is an optional query part
* `--paginated` allow to iterate on the results of the paginated endpoint (result formatted as ndjson)

For example, to get the running version:

```SH
tuleap-cli -s tuleap.net --path /api/version
```

Or, to list the public projects:

```SH
tuleap-cli -s tuleap.net --path /api/projects --paginated
```

Another example, to list the new requests for Tuleap product:

```SH
tuleap-cli -s tuleap.net --path /api/trackers/140/artifacts --query 'expert_query=status_id = "New"' --paginated
```

It is also possible to call endpoints with other HTTP method, like a `POST` to create an element.
We then have to use the `--method` and `--data` options.

```SH
tuleap-cli -s tuleap.example.com --path /api/frs_packages --method POST --data new_package.json
```

### Built-in endpoints

Supported commands are detailled in [the manual](docs/manual/tuleap-cli.md).

## Contributions

See [contribution notes](CONTRIBUTING.md).

## Authors

This project was initially created by Guilhem Bonnefille working at [CS GROUP - France](https://www.csgroup.eu).

## Acknowledgements

This project is possible due to the [go-tuleap](https://gitlab.com/csgroup-oss/go-tuleap/) Golang client to Tuleap's API.

## License

Licensed under the GNU GPL License, Version 3.0 or later.
See [COPYING](COPYING) to see the full text.
