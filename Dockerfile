#FROM registry.hub.docker.com/badouralix/curl-jq
#FROM dwdraju/alpine-curl-jq
#FROM alpine
FROM debian:12-slim

# hadolint ignore=DL3008
RUN apt-get update \
 && apt-get install --no-install-recommends -y jq \
 && rm -rf /var/lib/apt/lists/*

COPY bin/linux/amd64/tuleap-cli /usr/local/bin/