all: linux windows

#export PATH := /usr/lib/go-1.16/bin:$(PATH)
export GO_BUILD_LINKER_FLAGS :=  -ldflags "-X tuleap/tuleap-cli/cmd.version=dev"

windows:
	mkdir -p bin/windows/amd64
	GOOS=windows GOARCH=amd64 go build $(TAGS) $(GO_BUILD_LINKER_FLAGS) -o bin/windows/amd64 ./...

linux:
	mkdir -p bin/linux/amd64
	go build $(TAGS) $(GO_BUILD_LINKER_FLAGS) -o bin/linux/amd64 ./...

test:
	go test -v ./...

clean:
	go clean

version:
	go version

update:
	go get -v gitlab.com/csgroup-oss/go-tuleap@master

gendoc:
	go run scripts/gendoc/gendoc.go

docker: linux
	docker build . -t tuleap-cli