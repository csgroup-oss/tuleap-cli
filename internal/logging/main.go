// tuleap-cli, a CLI client to Tuleap
// Copyright (C) 2024 - CS GROUP - France
// Author: Guilhem Bonnefille

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package logging

import (
	"io"
	"log"
	"os"

	jww "github.com/spf13/jwalterweatherman"
)

var notepad *jww.Notepad

func init() {
	notepad = jww.NewNotepad(jww.LevelWarn, jww.LevelWarn, io.Discard, os.Stderr, "", log.Ldate|log.Ltime)
}

func Configure(debug bool, verbose bool) {
	if debug {
		notepad.SetLogThreshold(jww.LevelDebug)
	} else if verbose {
		notepad.SetLogThreshold(jww.LevelInfo)
	}
}

func Trace() *log.Logger {
	return notepad.TRACE
}

func Debug() *log.Logger {
	return notepad.DEBUG
}

func Info() *log.Logger {
	return notepad.INFO
}

func Warn() *log.Logger {
	return notepad.WARN
}

func Error() *log.Logger {
	return notepad.ERROR
}
