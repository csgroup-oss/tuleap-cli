package logging

import (
	"net/http"
	"net/http/httputil"
)

// LoggingTransport is a transport that logs requests and responses.
type LoggingTransport struct{}

// RoundTrip logs the request and response.
func (s *LoggingTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	bytes, _ := httputil.DumpRequestOut(r, true)
	Debug().Printf("Request:\n%s\n", bytes)

	resp, err := http.DefaultTransport.RoundTrip(r)
	// err is returned after dumping the response

	respBytes, _ := httputil.DumpResponse(resp, true)

	Debug().Printf("Response:\n%s\n", respBytes)

	return resp, err
}
